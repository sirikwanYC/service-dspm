import getEvaluationForm from './model/schemas/getEvaluationForm'
import insertEvaluationForm from './model/schemas/insertEvaluationForm'
import updateEvaluationForm from './model/schemas/updateEvaluationForm'

const checkEvaluationForm = (connection,result, body) => {
    if(result.length === 0){
        insertEvaluationForm(connection, body)
    }else{
        updateEvaluationForm(connection, body)
    }
}

const route = (server, connection) => { 
    server.post('/insert-evaluation-form', (req, res) => {
        getEvaluationForm(connection, checkEvaluationForm, req.body)
        res.status(200).end()
    })
}

export default route