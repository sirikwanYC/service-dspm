const insertEvaluationForm = (connection, body) => {
    if(body.assessor) {
        let sql = `INSERT INTO evaluation_result(cid, span_of_age, question_no, assessor, hosp_code) VALUES(${body.cid}, '${body.spanOfAge}', ${body.no}, '${body.assessor}', ${body.hospCode})`
        connection.query(sql)

    }else{
        let sql = `INSERT INTO evaluation_result(cid, span_of_age, question_no, question_type, result) VALUES(${body.cid}, '${body.spanOfAge}', ${body.no}, '${body.questionType}', '${body.result}')`
        connection.query(sql)
    }
}

export default insertEvaluationForm
